import Vue from 'vue/dist/vue.esm'
const __vue = {
  module: Vue,
  events: new Vue(),
  vars: {}
}

module.exports = __vue;