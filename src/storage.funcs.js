const fs = window.require('fs');
const fstorage = './bin/data/ytfstorage.dat';

const backup = {
  "settings": { },
  "playlists": {
    "PLr-T10zdqVfptaHsJ7SVME5htKQOoD7qd": {
      "update": 1
    }
  }
}

const storage = {
  async loadStorage() {
    try {
      let result = await this.loadFile(fstorage);
      this.data = JSON.parse(result);
    }
    catch(e) {
      console.error("An error occurred while trying to load the storage file");
      console.error(e);

      console.info("Attempting to intialize a default storage file");

      this.data = backup;
      await this.saveStorage();

      console.info(`A new storage file has been created`);
    }
  },
  async saveStorage() {
    try {
      if(typeof this.data != 'object') {
        return TypeError(`Internal storage variable could not be read`);
      }
      await this.writeFile(fstorage, JSON.stringify(this.data));
    }
    catch(e) {
      console.error("An error occurred while trying to save the storage file");
      console.error(e);
    }
  },
  loadFile(file) {
    return new Promise(function(resolve, reject) {
      fs.readFile(file, 'utf8', function(err, data) {
        if (err) reject(Error(err));
        else resolve(data);
      })
    })
  },
  writeFile(file, data) {
    return new Promise(function(resolve, reject) {
      fs.writeFile(file, data, 'utf8', function(err, data) {
        if (err) reject(Error(err));
        else resolve(data);
      })
    })
  }
}
module.exports = storage;