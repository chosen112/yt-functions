// styles
import "../styles/main.scss";

// vue and vue youtube lib
import Vue from './vue.loader.js';
import youtubeEmbed from 'vue-youtube-embed';

// jquery lib
import $ from './jquery.loader.js';

// vue modules
import vmTitleBar from './vue/titlebar.module.vue';
import vmBody from './vue/body.module.vue';
//import vmFooter from './vue/footer.module.vue';

import storage from './storage.funcs.js';
require('./youtube.funcs.js').init();

$(document).ready(async function() {
  Vue.module.use(youtubeEmbed)

  await storage.loadStorage();

  new Vue.module({
    el: 'section.__vue-module#vm-title-bar',
    template: '<vmTitleBar />',
    components: { vmTitleBar }
  })

  new Vue.module({
    el: 'section.__vue-module#vm-body',
    template: '<vmBody />',
    components: { vmBody }
  })

  /*new Vue.module({
    el: 'section.__vue-module#vm-footer',
    template: '<vmFooter />',
    components: { vmFooter }
  })*/
})