const youtube = {
  api: window.require('youtube-api'),
  init() {
    this.auth('AIzaSyAxSgaiM10dX9A7QEuhlZ7aKCjIbww1b3U');
  },
  auth(key) {
    youtube.api.authenticate({
      type: 'key',
      key: key
    })
  },
  async getPlaylist(playlist, options={}, result) {
    let args = {
      playlistId: playlist,
      maxResults: options.amount || 50,
      pageToken: options.page,
      part: "snippet,contentDetails"
    }

    if (!args.playlistId) {
      return SyntaxError('Playlist argument is empty!')
    }
    if (!args.pageToken) delete args.pageToken;
    
    function getPlaylistContent(args) {
      return new Promise(function(resolve, reject) {
        youtube.api.playlistItems.list(args, function (err, data) {
          if (err) reject(Error(err));
          else resolve(data);
        })
      })
    }

    let content = await getPlaylistContent(args);
    if(!result) {
      result = content;
    }
    else {
      result.items = result.items.concat(content.items);
    }

    if(options.complete && content.nextPageToken) {      
      options.page = content.nextPageToken;      
      return await youtube.getPlaylist(playlist, options, result);
    }

    return result;
  },
  getPlaylistInfo(playlist) {
    let args = {
      id: playlist,
      part: "snippet"
    }

    if (!args.id) {
      return SyntaxError('Playlist argument is empty!')
    }

    return new Promise(function(resolve, reject) {
      youtube.api.playlists.list(args, function(err, data) {
        if (err) reject(Error(err));
        else resolve(data);
      })
    })
  }
}

module.exports = youtube;