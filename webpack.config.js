const path = require('path');
const config = {
  entry: './src/main.js',
  output: {
    path: path.resolve(__dirname, 'bin'),
    filename: '[name].build.js'
  },
  resolve: {
    modules: [ path.join(__dirname, 'node_modules') ],
    alias: {
      'assets': path.resolve(__dirname, 'assets'),
      'styles': path.resolve(__dirname, 'styles'),
      'data': path.resolve(__dirname, 'bin', 'data')
    }
  },
  module: {
    rules: [
      {
        test: /\.vue$/,
        use: {
          loader: 'vue-loader'
        }
      },
      {
        test: /\.js$/,
        exclude: /(node_modules|bower_components)/,
        use: {
          loader: 'babel-loader',
          options: {
            presets: ['babel-preset-env'],
            plugins: ['transform-vue-jsx', 'babel-plugin-transform-runtime']
          }
        }
      },      
      {
        test: /\.(gif|png|jpe?g|svg)$/i,
        use: [
          {
            loader: 'file-loader',
            options: {
              name: '[path][name].[ext]',
              publicPath: './',
              outputPath: '../'
            }
          }
        ]
      },
      {
        test: /\.scss$/,
        use: [ 
          'style-loader', 
          'css-loader', 
          {
            loader: 'sass-loader',
            options: {
              includePaths: [path.resolve(__dirname, 'styles')],
              outputStyle: 'compressed'
            }
          }
        ]
      },
      {
        test: /\.(ttf|eot|woff|woff2)$/,
        loader: "file-loader",
        options: {
          name: "[path][name].[ext]",
          publicPath: './',
          outputPath: '../'
        },
      }
    ]
  }
}

module.exports = config;