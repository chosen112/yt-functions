const electron = require("electron");
const { app, BrowserWindow } = electron;
const path = require("path");
const url = require("url");

require('electron-referer')('https://www.youtube.com/');

const IMG_DIR = "/assets/images/";
const HTML_DIR = "/views/";

var win;
function createWindow() {
  const { width, height } = electron.screen.getPrimaryDisplay().workAreaSize
  win = new BrowserWindow({
    width: Math.floor(width * .9),
    minWidth: 1344,
    height: Math.floor(height * .9),
    minHeight: 743,
    icon: path.join(__dirname, IMG_DIR, "app.png"),
    frame: false,
    resizable: false,
    backgroundColor: '#220000',
    webPreferences: {
      //devTools: false // Completely remove chromium developer tools
    }
  })

  loadDev();
  loadWindow("yt-functions-main");
}

function loadDev() {
  win.openDevTools({ detach: true });
}

function loadWindow(page) {
  win.loadURL(url.format({
    pathname: path.join(__dirname, HTML_DIR, page + '.html'),
    protocol: "file:",
    slashes: true
  }))
}

app.on('ready', createWindow);
app.on('window-all-closed', () => {
  if (process.platform !== 'darwin') {
    app.quit();
  }
})